package com.binar.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.navigationtest.databinding.FragmentKeempatBinding


class FragmentKeempat : Fragment() {

    private var _binding: FragmentKeempatBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKeempatBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnKembali.setOnClickListener {

            if (binding.inputUsia.text.isNullOrEmpty()||binding.inputAlamat.text.isNullOrEmpty()||binding.inputPekerjaan.text.isNullOrEmpty()){
                Toast.makeText(requireContext(),"Data belum diisi lengkap",Toast.LENGTH_SHORT).show()
            }
            else{
                val args = this.arguments
                val namaa = args?.get("NAMA").toString()


                val aUsia = binding.inputUsia.text.toString()
                val aAlamat = binding.inputAlamat.text.toString()
                val aPekerjaan = binding.inputPekerjaan.text.toString()
                val vsb = "true"
                val aBundle = Bundle()
                val namaaa = "true"

                aBundle.putString("2","$namaa")
                aBundle.putString("USIA", aUsia)
                aBundle.putString("ALAMAT", aAlamat)
                aBundle.putString("PEKERJAAN", aPekerjaan)
                aBundle.putString("VSB", vsb)
                aBundle.putString("name",namaa)
                aBundle.putString("true", namaaa)

                val fragment = FragmentKetiga()
                fragment.arguments = aBundle
                fragmentManager?.beginTransaction()?.replace(R.id.navContainer,fragment)?.commit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}