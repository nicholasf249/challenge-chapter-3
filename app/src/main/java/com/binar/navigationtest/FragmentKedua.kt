package com.binar.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.navigationtest.databinding.FragmentKeduaBinding

class FragmentKedua : Fragment() {

    private var _binding: FragmentKeduaBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKeduaBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnHalamanKetiga.setOnClickListener {
            if (binding.inputNama.text.isNullOrEmpty()){
                Toast.makeText(requireContext(),"Masukkan nama!", Toast.LENGTH_SHORT).show()
            }
            else{
                val nama = binding.inputNama.text.toString()
                val mBundle = Bundle()
                mBundle.putString("NAMA", nama)
                val fragment = FragmentKetiga()
                fragment.arguments = mBundle
                fragmentManager?.beginTransaction()?.replace(R.id.navContainer,fragment)?.commit()
            }
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}