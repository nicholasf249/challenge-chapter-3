package com.binar.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.findNavController
import com.binar.navigationtest.databinding.FragmentPertamaBinding

class FragmentPertama : Fragment() {

    private var _binding: FragmentPertamaBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentPertamaBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding.btnHalamanKedua.setOnClickListener(){
            it.findNavController().navigate(R.id.action_fragmentPertama_to_fragmentKedua)
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}