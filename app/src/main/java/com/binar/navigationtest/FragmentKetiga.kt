package com.binar.navigationtest

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import com.binar.navigationtest.databinding.FragmentKetigaBinding

class FragmentKetiga : Fragment() {

    private var _binding: FragmentKetigaBinding? = null
    // This property is only valid between onCreateView and
// onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentKetigaBinding.inflate(inflater, container, false)
        val args = this.arguments
        val name = args?.get("NAMA").toString()

        binding.valueNama.text = "Nama anda: $name"
        val argssss =this.arguments
        val namaaa =argssss?.get("true")
        if(namaaa == "true"){
            val args = this.arguments
            val name = args?.get("name").toString()
            binding.valueNama.text = "Nama anda: $name"

        }
        return binding.root

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val args = this.arguments
        val textvsb = args?.get("VSB")

        if(textvsb == "true"){
            val argss = this.arguments
            val name = argss?.get("name").toString()
            binding.valueNama.text = "Nama Anda : $name"

            binding.valueUsia.visibility = View.VISIBLE
            binding.valueAlamat.visibility = View.VISIBLE
            binding.valuePekerjaan.visibility = View.VISIBLE

            val inputUsia = args?.get("USIA")
            val inputAlamat = args?.get("ALAMAT")
            val inputPekerjaan = args?.get("PEKERJAAN")

            val a = inputUsia.toString().toInt()
            val b = a.mod(2)

            if (b == 0){
                binding.valueUsia.text = "Usia Anda : $inputUsia, bernilai genap"
            }
            else {
                binding.valueUsia.text = "Usia Anda : $inputUsia, bernilai ganjil"
            }

            binding.valueAlamat.text = "Alamat Anda : $inputAlamat"
            binding.valuePekerjaan.text = "Pekerjaan Anda : $inputPekerjaan"
        }


//        val inputNama = args?.get("NAMA")
//        binding.valueNama.text = "Nama Anda: $inputNama"

        binding.btnHalamanKeempat.setOnClickListener {
            val args = this.arguments
            val name = args?.get("NAMA").toString()
            val aBundle = Bundle()

            aBundle.putString("NAMA", name)
            val fragment = FragmentKeempat()
            fragment.arguments = aBundle
            fragmentManager?.beginTransaction()?.replace(R.id.navContainer,fragment)?.commit()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        _binding = null
    }
}